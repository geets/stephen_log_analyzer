# log analyzer specification
## Goal of the application
### long term vision
Enable users to gather insight about certain predefined processes that are happening in their own applications.
To do this, log analyzer parses application logs and processes their contents so valuable information can be extracted.
The goal is for users to be able to easily configure the log analyzer to *understand* the application it analyzes and get back the information they need.
Examples for the type of information the user is looking for: timings between certain steps in a process or the number of times something happened or did NOT happen.

### specific use-case: SMPP
A specific example - and the use-case that will be implemented first - is reading logs of an application that talks the Short Message Peer-to-Peer Protocol (SMPP) with it's neighbour.

One part of the SMPP-specification describes how an SMS is transmitted. When **everything works fine** it comes down to the exchange of two messages:

`Sender: "here is a message I would like you to send: (additional data)"`

`Reciever: "I recieved the message and will process it"`

The first type of message is called `submit_sm` and the second is called `submit_sm_response`.
Associated with the above events of `submit_sm` and `submit_sm_response` are specific loglines.
Here is a real life example of these two loglines:
````
2021-12-10 23:58:02.969: TRACE: PDULoggingFilter.messageSent(50): [TX-01:00001128AF] [0248] SEND(1835) [submit_sm]: REDACTED
2021-12-10 23:58:05.704: TRACE: PDULoggingFilter.messageReceived(31): [TX-01:00001128D0] [0248] RECI(1835) [submit_sm_resp]: SubmitSmResp[message_id=64488070,command_length=0x19,command_id=0x80000004(submit_sm_resp),command_status=0x00000000(ESME_ROK),sequence_number=0x72b]
````

But what happens when **no submit_sm_response** is received within a certain time?
The SMPP-Application will call a timeout and retransmit the message.
In the logs:
````
2021-12-10 23:58:02.969: TRACE: PDULoggingFilter.messageSent(50): [TX-01:00001128AF] [0248] SEND(1835) [submit_sm]: REDACTED
2021-12-10 23:58:12.969:  WARN: CommandStatusStrategy.execute(59): [TX-01:Timeout] Submit failed for seq-nr 1835: TIMEOUT [00000400] (Carrier timeout)
````

A fouth type of logline gets logged when a `submit_sm_response` arrives after it already got timeouted:

````
2021-12-10 23:58:20.969: ERROR: SMSCCommunication.received(340): [TX-01:00001128B6] No callback for sequence-nr: 1835
````