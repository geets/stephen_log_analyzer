import pytest
from schema import SchemaError
from log_parser import (
    parse_logtime,
    timeout,
    late_submit_sm_response,
    sms,
)


@pytest.fixture
def timeout_one_logline():
    return "2001-01-01 01:01:10.001:  WARN: CommandStatusStrategy.execute(59): [TX-01:Timeout] Submit failed for seq-nr 1: TIMEOUT [00000400] (Carrier timeout)"


@pytest.fixture
def timeout_one_dict(timeout_one_datetime):
    return {
        "seq_nr": "1",
        "timestamp": "2001-01-01 01:01:10.001",
        "timestamp_datetime": timeout_one_datetime,
        "parser": "timeout",
    }


@pytest.fixture
def late_submit_sm_response_one_logline():
    return "2001-01-01 01:01:15.001: ERROR: SMSCCommunication.received(340): [TX-01:000021C843] No callback for sequence-nr: 1"


@pytest.fixture
def late_submit_sm_response_one_dict(late_submit_sm_response_one_datetime):
    return {
        "seq_nr": "1",
        "timestamp": "2001-01-01 01:01:15.001",
        "timestamp_datetime": late_submit_sm_response_one_datetime,
        "parser": "late_submit_sm_response",
    }


@pytest.fixture
def definitely_wrong_logline():
    return "2001-01-01 01:01:10.001:  RANDOM STUFF"


@pytest.fixture
def submit_sm_one_dict(submit_sm_one_datetime):
    return {
        "seq_nr": "1",
        "timestamp": "2001-01-01 01:01:01.001",
        "timestamp_datetime": submit_sm_one_datetime,
        "parser": "submit_sm",
    }


### DATETIMEs:


@pytest.fixture
def submit_sm_one_datetime():
    return parse_logtime("2001-01-01 01:01:01.001")


@pytest.fixture
def timeout_one_datetime():
    return parse_logtime("2001-01-01 01:01:10.001")


@pytest.fixture
def late_submit_sm_response_one_datetime():
    return parse_logtime("2001-01-01 01:01:15.001")


# PARSER OUTPUTS:


@pytest.mark.parametrize(
    "parser,logline",
    [
        (timeout, "definitely_wrong_logline"),
        (late_submit_sm_response, "definitely_wrong_logline"),
    ],
)
def test_parsers_return_none_if_no_match(parser, logline, request):
    logline = request.getfixturevalue(logline)
    assert parser(logline) is None


@pytest.mark.parametrize(
    "parser,logline,expected_output",
    [
        (timeout, "timeout_one_logline", "timeout_one_dict"),
        (
            late_submit_sm_response,
            "late_submit_sm_response_one_logline",
            "late_submit_sm_response_one_dict",
        ),
    ],
)
def test_parsers_return_necessary_keys(parser, logline, expected_output, request):
    logline = request.getfixturevalue(logline)
    expected_output = request.getfixturevalue(expected_output)
    assert parser(logline) == expected_output


@pytest.mark.parametrize(
    "parser,logline,expected_output",
    [
        (timeout, "timeout_one_logline", "timeout_one_dict"),
        (
            late_submit_sm_response,
            "late_submit_sm_response_one_logline",
            "late_submit_sm_response_one_dict",
        ),
    ],
)
def test_parsers_return_additional_kwargs(parser, logline, expected_output, request):
    additional_data = {"logline": "10", "filename": "/var/log/sms.log"}
    logline = request.getfixturevalue(logline)
    expected_output = {**request.getfixturevalue(expected_output), **additional_data}
    assert parser(logline, **additional_data) == expected_output


# SMS class:


# @pytest.mark.parametrize(
#    "keyword", ["seq_nr", "parser", "timestamp", "timestamp_datetime"]
# )
# def test_sms_fails_at_init_if_mandatory_keys_missing(
#    keyword, late_submit_sm_response_one_dict
# ):
#    del late_submit_sm_response_one_dict[keyword]
#    with pytest.raises(KeyError):
#        sms(late_submit_sm_response_one_dict)


def seq_test_sms_fails_at_init_if_mandatory_keys_missing(
    late_submit_sm_response_one_dict,
):
    #    late_submit_sm_response_one_dict["seq_nr"] = 7
    with pytest.raises(Exception):
        sms(late_submit_sm_response_one_dict)
