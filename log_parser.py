import datetime
import re
from typing import Any, TextIO, Dict, Union, List
from schema import Schema, And, Use, Optional, SchemaError

"""
Find all loglines that represent different steps in the same transaction.
E.g. the following two steps belong to the same transaction (A):
send-message-A 
receive-answer-A 

And save the results in a way the whole transaction can be investigated later
"""


class sms:
    """
    holds 4 dictionaries that each represent one specific step in the process of delivering an sms:
            "submit_sm"
            "submit_sm_response"
            "timeout" (optional)
            "late_submit_sm_response" (optional)

    Each dict needs the same seq_nr and an increasing timestamp.

    """

    schema_timeout = Schema(
        {
            "parser": lambda n: n
            in [
                "submit_sm",
                "submit_sm_response",
                "timeout",
                "late_submit_sm_response",
            ],
            "timestamp": str,
            "timestamp_datetime": datetime.date,
            "seq_nr": str,
            Optional(str): object,
        }
    )

    #    primary_key: str = "seq_nr"

    def __init__(self, logevent: Dict[str, Any]) -> None:
        self.events: Dict[str, Union[Dict[str, Any], None]] = {
            "submit_sm": None,
            "submit_sm_response": None,
            "timeout": None,
            "late_submit_sm_response": None,
        }
        validated = self.validate_logevent(logevent)
        self.events["parser"] = validated

    #        primary_key_value = None

    def validate_logevent(self, logevent: Dict[str, Any]) -> Dict[str, Any]:
        """
        Validation with schema library:
        schema_timeout = Schema( { "parser": str, "timestamp": str, "timestamp_datetime": datetime.date, "seq_nr": str, })
        data = { "seq_nr": "1", "timestamp": "2001-01-01 01:01:10.001", "timestamp_datetime": timeout_one_datetime, "parser": "timeout", }

        schema_timeout.is_valid(date) # returns true/false

        Args:
            logevent (Dict[str, Any]): represents a step in the lifecycle of delivering an sms.

        Returns:
            Dict[str, Any]: returns the validated logevent if it is valid. Else throws an exception which points to the reason why it failed to validate.
        """
        return self.schema_timeout.validate(logevent)


def parse_logtime(time_string) -> datetime:
    return datetime.datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S.%f")


# PARSERS:


def submit_sm(line_to_parse: str, **kw: Any) -> Union[Dict[str, Any], None]:
    # parse line regex
    #    return {timestamp: datetime, seq_nr: "number", line_str: "line_str", filename: "filename", line_nr: "number"}
    # if no finding return None
    pass


def submit_sm_response(line_to_parse: str, **kw: Any) -> Union[Dict[str, Any], None]:
    # parse line regex
    #    return {timestamp: datetime, seq_nr: "number", line_str: "line_str", filename: "filename", line_nr: "number"}
    # if no finding return None
    pass


def timeout(line_to_parse: str, **kw: Any) -> Union[Dict[str, Any], None]:
    if regex_obj := re.match(
        r"(.{23})(.*)seq-nr ([0-9]*):(.*) \(Carrier timeout\)", line_to_parse
    ):
        date, seq_nr = regex_obj.group(1, 3)
        return {
            "parser": "timeout",
            "timestamp": date,
            "timestamp_datetime": parse_logtime(date),
            "seq_nr": seq_nr,
            **kw,
        }
    else:
        return None


def late_submit_sm_response(line_to_parse, **kw) -> Union[Dict[str, Any], None]:
    if regex_obj := re.match(
        r"(.{23})(.*): ERROR: (.*) sequence-nr: ([\d]*)", line_to_parse
    ):
        date, seq_nr = regex_obj.group(1, 4)
        return {
            "parser": "late_submit_sm_response",
            "timestamp": date,
            "timestamp_datetime": parse_logtime(date),
            "seq_nr": seq_nr,
            **kw,
        }
    else:
        return None


def save_sms_to_dict(adict: dict, logfile: TextIO) -> None:
    for line_nr, line in enumerate(logfile):
        if finding := submit_sm(line, line_nr):
            adict[finding["seq_nr"]] = Submit_sm(
                finding
            )  # __init__ of submit_sm () should take a dict from the parser.
        else:
            print("nothing found")


"""
13:00:01 submit_sm seq_nr: 1
13:00:02 submit_sm_response seq_nr: 1
13:00:02 submit_sm seq_nr: 2
13:00:12 submit_sm_timeout seq_nr: 2
13:00:13 submit_sm seq_nr: 3
13:00:23 submit_sm_timeout seq_nr: 3
13:00:25 submit_sm_callback_delayed seq_nr: 3
13:00:26 submit_sm seq_nr: 4
14:00:26 submit_sm seq_nr: 4
14:00:32 submit_sm_response seq_nr: 4



Two ways to go about parsing the whole log:

Run one (or multiple) multi-parser over the file line by line.
If it finds the first step in a process, it removes this line from the whole text and continues to search for the next step which shares the same ID.

Pro: could be possible to use multithreading
Pro: correlation happens in the multi-parser
Contra: many lines will be read multiple times by different instances of the parser.

2:
Feed each line to exactly 4 different parsers. Here the parsers are specialized to find one specific step in the process. (as opposed to idea 1: where one parser 
was able to find any of the 4 steps)
If one parser finds something it creates an object with extracted info of this logline and returns it.
The caller of the parsers needs to correlate the results of the different parsers.
"""
